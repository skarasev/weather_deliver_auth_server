package ru.madela.pattern.weather_deliver_oauth_server.util.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrantTypeDto {
    private UUID id;
    private String name;
}
