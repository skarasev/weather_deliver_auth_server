package ru.madela.pattern.weather_deliver_oauth_server.tool.details.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model.IUserDao;
import ru.madela.pattern.weather_deliver_oauth_server.tool.details.model.WeatherDeliverUserDetails;

@Service
public class WeatherDeliverUserDetailsService implements UserDetailsService {
    private final IUserDao userDaoService;

    public WeatherDeliverUserDetailsService(IUserDao userDaoService) {
        this.userDaoService = userDaoService;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return new WeatherDeliverUserDetails(userDaoService.getByName(s));
    }
}
