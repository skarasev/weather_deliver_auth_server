package ru.madela.pattern.weather_deliver_oauth_server.tool.details.service;

import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model.IClientDao;
import ru.madela.pattern.weather_deliver_oauth_server.tool.details.model.WeatherDeliverClientDetails;

import java.util.UUID;

public class WeatherDeliverClientDetailsService implements ClientDetailsService {
    private final IClientDao clientDaoService;

    public WeatherDeliverClientDetailsService(IClientDao clientDaoService) {
        this.clientDaoService = clientDaoService;
    }

    @Override
    public ClientDetails loadClientByClientId(String s) throws ClientRegistrationException {
        return new WeatherDeliverClientDetails(clientDaoService.getById(UUID.fromString(s)));
    }
}
