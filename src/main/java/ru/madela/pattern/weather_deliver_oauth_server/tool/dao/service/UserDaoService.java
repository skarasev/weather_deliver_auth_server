package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.User;
import ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model.IUserDao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
public class UserDaoService implements IUserDao {
    private final EntityManager entityManager;

    public UserDaoService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public User getByName(String name) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
        Root<User> userRoot = userQuery.from(User.class);
        Predicate usernamePredicate = cb.equal(userRoot.get("username"), name);
        return entityManager
                .createQuery(userQuery
                        .select(userRoot)
                        .where(usernamePredicate))
                .getSingleResult();
    }
}
