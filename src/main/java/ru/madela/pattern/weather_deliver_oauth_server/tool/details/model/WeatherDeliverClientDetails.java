package ru.madela.pattern.weather_deliver_oauth_server.tool.details.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.Client;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.GrantType;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.Scope;

import java.util.*;
import java.util.stream.Collectors;

public class WeatherDeliverClientDetails implements ClientDetails {
    private final Client client;

    public WeatherDeliverClientDetails(Client client) {
        this.client = client;
    }

    @Override
    public String getClientId() {
        return client.getId().toString();
    }

    @Override
    public Set<String> getResourceIds() {
        return Collections.singleton("1234");
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return client.getSecret();
    }

    @Override
    public boolean isScoped() {
        return true;
    }

    @Override
    public Set<String> getScope() {
        return client
                .getScopeSet()
                .stream()
                .map(Scope::getName)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return client
                .getGrantTypeSet()
                .stream()
                .map(GrantType::getName)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return Collections.singleton("http://localhost:8081/login");
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return 1800;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return 3600;
    }

    @Override
    public boolean isAutoApprove(String s) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return new HashMap<>();
    }
}
