package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

public interface IUpdatable<T> {
    Integer update(T t);
}
