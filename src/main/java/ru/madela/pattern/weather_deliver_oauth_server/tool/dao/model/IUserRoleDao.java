package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.Role;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.User;

import java.sql.SQLException;
import java.util.Set;

public interface IUserRoleDao extends IDao<Role> {
    Set<Role> getUserRoles(User user) throws SQLException;
}
