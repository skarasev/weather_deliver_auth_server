package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

public interface IDeletable<T> {
    Integer delete(T t);
}
