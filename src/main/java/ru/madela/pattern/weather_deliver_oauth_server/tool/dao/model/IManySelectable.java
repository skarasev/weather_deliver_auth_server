package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

import java.util.List;

public interface IManySelectable<T> {
    List<T> getAll();
}