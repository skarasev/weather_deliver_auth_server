package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.Client;
import ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model.IClientDao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.UUID;

@Service
public class ClientDaoService implements IClientDao {
    private final EntityManager entityManager;

    @Autowired
    public ClientDaoService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Client getById(UUID id) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Client> clientQuery = cb.createQuery(Client.class);
        Root<Client> clientRoot = clientQuery.from(Client.class);
        Predicate idPredicate = cb.equal(clientRoot.get("id"), id);
        return entityManager.
                createQuery(clientQuery
                        .select(clientRoot)
                        .where(idPredicate))
                .getSingleResult();
    }
}
