package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

public interface IInsertable<T> {
    Integer insert(T t);
}
