package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

public interface IDao<T> extends IMonoSelectable<T>, IDeletable<T>,
        IUpdatable<T>, IInsertable<T>, IManySelectable<T> {
}
