package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.User;

import java.sql.SQLException;

public interface IUserDao {
    User getByName(String name);
}
