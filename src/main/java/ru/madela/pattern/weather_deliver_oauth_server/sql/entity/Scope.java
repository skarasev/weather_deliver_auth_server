package ru.madela.pattern.weather_deliver_oauth_server.sql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "scope_table")
public class Scope {

    @Id
    private UUID id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;
}
