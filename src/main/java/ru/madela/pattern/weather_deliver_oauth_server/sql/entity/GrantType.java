package ru.madela.pattern.weather_deliver_oauth_server.sql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "grant_type_table")
public class GrantType {

    @Id
    private UUID id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;
}
